# Nettoyage des jeux de données sur l'empreinte environnemental des ingrédients

## Télécharger les données

Placer dans un dossier data, le fichier "tableur pour les produits alimentaires"

```
mkdir data
wget https://data.ademe.fr/data-fair/api/v1/datasets/agribalyse-31-synthese/metadata-attachments/AGRIBALYSE3.1_produits%20alimentaires_2.xlsm -P data
```
## Notion abordées


Pandas: 
- import de données depuis un fichier excel
- selection de ligne et colonnes
- résumé de donnée par agrégation
