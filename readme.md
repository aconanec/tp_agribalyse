# Projet d'analyse de données en python : l'empreinte carbone des aliments 

## Présentation de Agribalise

Les données de l'empreinte carbone proviennent de l'ADEME au travers d'un travail se nommant agribalise. Agribalise met à disposition l'empreinte écologique sur plusieurs critères (CO2, emprise au sol, couche d'ozone, etc.) via une analyse en cycle de vie. Pour plus de détail voir la documentation :

https://doc.agribalyse.fr/documentation/acces-donnees


## Consigne du projet

Les consignes du projet sont données sur ce lien : https://docs.google.com/document/d/1tqcwO99cVwcqaweJIiY91MyA1OvHCX1w4m9qaX7y1t4/edit?usp=sharing

## Consignes reparties en étapes 

### 0- Set up de l'environnement

Avant de commencer l'analyse des données, il est nécessaire de préparer son environnement de travail et adopter quelques bonnes pratique de développement. Le projet sera réalisé sur l'éditeur de text VS code.

- installez VS code.
- créez un environnement virtuel pour la gestion des dépendances.
- initiez le versionning du code avec git.

### 1- Import et nettoyage des jeux de données

La première étape de se projet consiste à importer la base de données fournie par agribalyse, la nettoyer et preparer les fichiers qui seront analysés ensuite.

- Téléchargez le fichier de données avec le lien https://data.ademe.fr/data-fair/api/v1/datasets/agribalyse-31-synthese/metadata-attachments/AGRIBALYSE3.1_produits%20alimentaires_2.xlsm .
- Importez le fichier excel. Créez un fichier csv à partir de l'onglet "Synthese" en supprimant les premières ligne inutiles. De même pour l'onglet "Detail etape", en choisissant un indicateur écologique à étudier.
- Réalisez un audit des données. Vérifier par exemple si le code AGB et le code CIQUAL sont des doublons.


### 2- Visualisation des empreintes des ingrédients

La seconde étape du projet consiste à analyser l'empreinte écologique des aliments individuellement, via de la visualisation des données. Le but est d'identifier l'impact écologique de différents groupes d'aliments et au cours des différentes étapes de production.

- Comparez l'impact moyen d'un indicateur écologique en fonction de l'étape de production pour chaque catégorie d'aliment.
    - De même, mais cette fois en prenant en compte la variabilité au sein des groupes d'aliments.
- Réalisez un zoom sur la catégorie "viandes, œufs, poissons".
- De même avec la sous-catégorie "viande crues".
- Faites un regroupement de modalités pour obtenir une meilleur visualisation (par espèce animal par exemple).
- Quelle est la corrélation entre les différents indicateurs écologiques ?


#### 3- Empreinte d'une recette

Dans la troisième phase du projet, vous analyserez non plus l'impact écologique des ingrédients mais celui d'une recette. Pour cela vous utiliserez une API, qui a été créée de manière ad hoc pour ce TP, et qui vous permettra à partir d'un nom de recette, d'obtenir la liste des ingrédients et de leur quantité. 

- Récupérer un token pour avoir accès à l'API.
- Stocker le token dans un fichier crédential (pensez à ajouter ce fichier à votre .gitignore !!)
- Prenez connaissance de la documentation de l'API via : http://162.19.108.77:8000/docs
- Faites une requête POST pour obtenir la liste des recettes (1ere page) en donnant via le chemin "recipe_urls" de l'API.
- Passez l'un des urls obtenus au chemin "ingredients_marmiton" de l'API pour obtenir la liste des ingrédients de la recette.
- Passez cette liste d'ingrédients au chemin "ingredients_recipe" de l'API, pour obtenir le code AGB de l'ingrédient et la masse associé à la recette.
- Fusionner les données écologiques de synthese grâce à la correspondance du code AGB.
- Réaliser une visualisation de l'impact carbone des ingrédients (en quantité et en proportion au sein de la recette).


#### 4- Scrapping des données marmiton

Enfin, l'objectif de cette dernière partie est que vous alliez vous même chercher la liste des ingrédients d'une recette en réalisant un webscrapping du site de cuisine marmiton.

- Écrivez une fonction qui prend en entrée un nom de recette et qui retourne une liste d'urls vers recettes de la première page de résultat.
- Écrivez une fonction qui prend en entrée l'url de la page d'une recette et qui donne en sortie une liste des ingrédients (avec instruction et quantité). 


#### 5- Conclusion

À la fin de ce projet, prenez un moment pour réfléchir aux résultats obtenus et aux évolutions du projet :

- Quels ont été les résultats les plus significatifs que vous avez obtenus lors de l'analyse des données sur l'empreinte carbone des aliments ?
- Quelles améliorations ou extensions pourriez-vous apporter à ce projet si vous aviez plus de temps ou de ressources ?

Faites aussi un état des lieux des défis rencontrés et des leçons apprises tout au long du projet :

- Quels défis avez-vous rencontrés lors de l'importation, du nettoyage et de la visualisation des données ? Comment les avez-vous surmontés ?
- Qu'avez-vous appris sur la gestion des dépendances, l'utilisation de Git pour le versionnement du code et l'environnement de développement dans VS Code ?
- Quelles compétences techniques ou non techniques avez-vous développées au cours de ce projet ?
- Comment ce projet a-t-il renforcé votre compréhension de l'analyse de données et de la programmation en Python ?

## Déroulement du projet

Vous travaillerez en groupe de 3-4 étudiants.

Durant ce projet vous devrez faire preuve d'autonomie en utilisant les différents support de documentation à votre disposition (cours, documentation officiel, stackoverflow). L'utilisation de chatGPT n'est pas exclue, mais utilisez cette ressource pour vous aider à comprendre comment fonctionne certaines notions ou fonctions, davantage que pour lui demander de créer le code à votre place. Si un blocage apparait, n'hésitez pas à me demander un coup de pouce :)

En fin de journée (16h ?), deux groupes présenterons leur travail de la journée et nous échangerons ensemble sur leur code.

