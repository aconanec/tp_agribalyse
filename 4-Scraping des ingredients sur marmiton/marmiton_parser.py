import requests
from bs4 import BeautifulSoup
import openai
import os
import pandas as pd

openai.api_key = os.environ.get("OPENAI_API_KEY")



def get_recipes_url(recipe_name):
    endpoint = "https://www.marmiton.org"
    params = {"aqt": recipe_name}
    response = requests.get(f"{endpoint}/recettes/recherche.aspx", params=params)
    
    if response.status_code:
        # extract html content
        html_content = response.text

        # Parse the HTML content
        soup = BeautifulSoup(html_content, "html.parser")
        
        # get the targeted links of the receipes
        links = soup.find_all("a", class_="MRTN__sc-1gofnyi-2 gACiYG")

        return [endpoint + link.get("href") for link in links]
        
    else:
        return None


def get_ingredient_from_receipe(url_recipe):
    
    response = requests.get(url_recipe)
    if response.status_code:
        # extract html content
        html_content = response.text

        # Parse the HTML content
        soup = BeautifulSoup(html_content, "html.parser")

        # get the targeted div where ingredients text is
        ingredients = soup.find_all("div", class_="RCP__sc-vgpd2s-1 fLWRho")
        
        return [ingredient.get_text() for ingredient in ingredients]
    else:
        return None


def get_mass_ingredients(receipe_name, ingredients_list):

    question_asked = f"""
    You are a chef in a restaurant and you have to prepare a recipe for {receipe_name} given the list of ingredients: {str(ingredients_list)}.
    To prepare the recipe, you need to estimate the weight of each ingredient in g.
    If an ingredient is without a specific unit, estimate the weight of that ingredient based on the recipe's context or a typical weight of similar ingredients.
    If only the volume is given, estimate the mass based on the density of the ingredient or a typical conversion factor.
    I absolutly need the mass to be estimated as a number, no text is allowed!! If no indication is given, please provide one based on your knowledge of chef.
    Ensure that the response includes only numeric values for the mass.
    Provide the result as a dictionaries where the key is the ingredient_name and the value the mass.
    Provide response in JSON format, no explaination because I need to parse it automatically. 
    """

    tentative = 2
    while tentative > 0:
        result = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages=[
                {"role": "user", "content": question_asked},
            ],
            temperature=0.1
        )

        output = result.choices[0]["message"]["content"].replace("null", "None")
        res = None
        try:
            mass_ingredients = eval(output)
            if all([isinstance(mass, int) or isinstance(mass, float) for mass in mass_ingredients.values()]):
                tentative = 0
                res = mass_ingredients
            else:
                tentative -=1
        except:
            tentative -=1

    return res


def get_ingredient_group(list_ingredients_names, list_groups):
    list_groups_df = pd.DataFrame({"id": range(len(list_groups)), "name": list_groups})
    
    question_asked = f"""
    for each ingredient : {list_ingredients_names}, give me the corresponding group of ingredients given in the following list : {list_groups_df.to_json(orient="records")}.
    give it in JSON format, without side notes, because I need to parse it automatically.
    Provide the result as a dictionaries where the key is the ingredient_name and the id of the group.
    """


    tentative = 1
    res = None
    while tentative > 0:
        result = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
                {"role": "user", "content": question_asked},
            ],
        temperature=0.1
        )

        output = result.choices[0]["message"]["content"].replace("null", "None")
        try:
            ingredients_group_output = eval(output)
            if all([isinstance(id, int) for id in ingredients_group_output.values()]):
                tentative = 0

                # convert a to dataframe
                ingredients_group_output = pd.DataFrame({"ingredient": list(ingredients_group_output.keys()), "id": list(ingredients_group_output.values())})

                # merge a_df with list_groups_df
                res = ingredients_group_output.merge(list_groups_df, on="id", how="left")[["ingredient", "name"]]
                res.rename(columns={"name": "ingredient_group"}, inplace=True)
                
            else:
                tentative -=1
        except:
            tentative -=1

    return res

def get_ingredient_subgroup(ingredient_name, list_subgroups):
    list_groups_df = pd.DataFrame({"id": range(len(list_subgroups)), "name": list_subgroups})
    
    question_asked = f"""
    For the ingredient {ingredient_name}, give me the corresponding group of ingredients given in the following list : {list_groups_df.to_json(orient="records")}.
    Because I need to parse it automatically, I want you to give me only the id of the group.
    Provide the result as JSON format of a dictionary with one key : "id".
    """

    tentative = 3
    res = None
    while tentative > 0:
        result = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
                {"role": "user", "content": question_asked},
            ],
        temperature=0.1
        )

        output = result.choices[0]["message"]["content"].replace("null", "None")
        try:
            ingredients_id_output = eval(output)["id"]
            if isinstance(ingredients_id_output, int):
                tentative = 0

                res = list_groups_df[list_groups_df["id"] == ingredients_id_output]["name"].values[0] 
                
            else:
                tentative -=1
        except:           
            tentative -=1

    return res



agribalyse = pd.read_csv('data/ingredients.csv', sep=';')

def get_ingredients_code(list_ingredients_names):

    
    # get the group of each ingredient
    ingredients_group = get_ingredient_group(list_ingredients_names, agribalyse["Groupe d'aliment"].drop_duplicates().values.tolist())

    # get the sub group of each ingredient
    sub_groups = []
    for ingredient_key in list_ingredients_names:
        # get the group name 
        group_name = ingredients_group[ingredients_group["ingredient"] == ingredient_key]["ingredient_group"].values[0] 
        
        # filter the agribalyse dataframe to get the ingredient name and the group name
        subgroups_i = agribalyse[agribalyse["Groupe d'aliment"] == group_name][["Sous-groupe d'aliment"]].drop_duplicates()["Sous-groupe d'aliment"].values.tolist()  
        
        sub_groups.append( get_ingredient_subgroup(ingredient_key, subgroups_i) )

    ingredients_group["ingredient_subgroup"] = sub_groups

    # get the product name of each ingredient
    product_std = []
    for ingredient_key in list_ingredients_names:
        # get the group name 
        subgroup_name = ingredients_group[ingredients_group["ingredient"] == ingredient_key]["ingredient_subgroup"].values[0] 
        
        # filter the agribalyse dataframe to get the ingredient name and the group name
        subgroups_i = agribalyse[agribalyse["Sous-groupe d'aliment"] == subgroup_name]["Nom du Produit en Français"].values.tolist()  
        
        # print(subgroups_i)
        product_std.append( get_ingredient_subgroup(ingredient_key, subgroups_i) )


    ingredients_group["product_std"] = product_std

    ingredients_group["Code\nAGB"] = [
    agribalyse[agribalyse["Nom du Produit en Français"] == product_std]["Code\nAGB"].values[0] 
    for product_std in ingredients_group["product_std"]]

    return ingredients_group