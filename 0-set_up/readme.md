# Préparation de l'environnement de travail

## Installation de VS code

Installez VS code avec ce lien de téléchargement https://code.visualstudio.com/download

## Création d'un environnement virtuel

```
python -m venv venv
source venv/bin/activate
pip freeze
```

## Installation des dépendances

```
pip install pandas numpy plotly requests ipykernel openpyxl scikit-learn bs4
```

## Lister les dépendances dans un fichier requirements.txt

```
pip freeze > requirements.txt
```
